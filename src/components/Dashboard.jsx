import React from "react";
import NavbarVendedor from "./vendedor/NavbarVendedor";
import NavbarCliente from "./cliente/NavbarCliente";

const Dashboard = () => {

  const [title, setTitle] = React.useState("");
  const [name, setName] = React.useState("")
  const [description, setDescription] = React.useState("");

  React.useEffect(() => {
    getDashboard();
  });

  const getDashboard = () => {
    if (localStorage.getItem("role") === "vendedor") {
      setTitle("Bienvenido");
      setName("Vendedor")
      setDescription("Bienvenido a tu panel de control. Aqui puedes listar, agregar, editar y desactivar clientes.");
    } else if (localStorage.getItem("role") === "cliente") {
      setTitle("Bienvenido");
      setName("Cliente")
      setDescription("Bienvenido a tu panel de control. Aqui puedes pagar ver tu historial de pagos y ver tus datos personales.");
    }
  }
  
  return (
    <div>
      {localStorage.getItem("role") === "vendedor" ? (
        <NavbarVendedor />
      ) : (
        <NavbarCliente />
      )}
      <div>
        <br />
        <div className="flex justify-center">
          <div className="block mx-6 w-screen p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
            <div className="flex justify-center">
              <p className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
                {title}
              </p>
            </div>
            <div className="flex justify-center">
              <span className="mb-4 text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-pink-500 to-orange-400">
                {name}
              </span>
            </div>
          </div>
        </div>

        <hr className="my-4 mx-auto w-2/3 h-1 bg-gray-100 rounded border-0 md:my-10 dark:bg-gray-700" />

        <div className="flex justify-center">
          <div className="block mx-6 w-screen p-6 text-center bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
            <div className="flex justify-center">
              <blockquote className="p-4 my-4 bg-gray-50 border-l-4 border-gray-300 dark:border-gray-500 dark:bg-gray-800">
                <p className="text-3xl italic font-medium leading-relaxed text-gray-900 dark:text-white">
                  {description}
                </p>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
