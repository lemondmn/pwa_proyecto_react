import React from "react";
import axios from "../../api/axios";
import { Table, Button } from "flowbite-react";
import NavbarVendedor from "./NavbarVendedor";
import { Link, useNavigate } from "react-router-dom";

const VendedorIndex = () => {
  const [clientes, setClientes] = React.useState([]);
  const navigate = useNavigate();

  React.useEffect(() => {
    getClientes();
  });

  const getClientes = async (e) => {
    try {
      const response = await axios.get(
        "/vendedores/" + localStorage.getItem("id"),
        {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
            "Content-Type": "application/json",
          },
        }
      );
      setClientes(response.data.usuarios);
      console.log(response.data.usuarios);
    } catch (err) {
      console.log(err);
    }
  };

  const handleNavigate = () => {
    navigate("/vendedor/clientes/nuevo");
  };

  function handleDeactivate(id) {
    const deactivate = async () => {
      try {
        const response = await axios.delete("/vendedores/" + id, {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
            "Content-Type": "application/json",
          },
        });
        console.log(response);
        getClientes();
      } catch (err) {
        console.log(err);
      }
    }
    deactivate();
  }

  return (
    <div>
      <NavbarVendedor />
      <br />
      <div className="flex justify-center">
        <div className="block mx-6 w-auto p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
          <p className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            Clientes
          </p>
        </div>
      </div>
      <br />
      <div className="flex justify-center">
        <button
          onClick={handleNavigate}
          className="text-white bg-gradient-to-br from-pink-500 to-orange-400 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2 w-96"
        >
          Cliente Nuevo
        </button>
      </div>
      <div className="flex w-full">
        <div className="w-full py-12 flex items-center justify-center">
          <div className="min-w-min px-12">
            <Table className="w-full table-fixed text-sm text-left text-gray-500 dark:text-gray-400">
              <Table.Head>
                <Table.HeadCell>Nombre</Table.HeadCell>
                <Table.HeadCell>A. Paterno</Table.HeadCell>
                <Table.HeadCell>A. Materno</Table.HeadCell>
                <Table.HeadCell>Direccion</Table.HeadCell>
                <Table.HeadCell>Telefono</Table.HeadCell>
                <Table.HeadCell>Email</Table.HeadCell>
                <Table.HeadCell>Estado</Table.HeadCell>
                <Table.HeadCell>Acciones</Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {clientes.map((cliente) => (
                  <Table.Row
                    key={cliente.id}
                    className="bg-white dark:border-gray-700 dark:bg-gray-800"
                  >
                    <Table.Cell>{cliente.nombre}</Table.Cell>
                    <Table.Cell>{cliente.apaterno}</Table.Cell>
                    <Table.Cell>{cliente.amaterno}</Table.Cell>
                    <Table.Cell>{cliente.direccion}</Table.Cell>
                    <Table.Cell>{cliente.telefono}</Table.Cell>
                    <Table.Cell>{cliente.email}</Table.Cell>
                    <Table.Cell>
                      {cliente.estado_id === 1 ? (
                        <span className="bg-green-200 text-green-600 py-1 px-3 rounded-full text-xs">
                          Activo
                        </span>
                      ) : (
                        <span className="bg-red-200 text-red-600 py-1 px-3 rounded-full text-xs">
                          Inactivo
                        </span>
                      )}
                    </Table.Cell>
                    <Table.Cell>
                      <Link to={`/vendedor/clientes/editar/${cliente.id}`}>
                        <Button className="w-40 bg-purple-200 text-purple-600 hover:bg-purple-600 hover:text-purple-200 py-1 px-3 rounded-full text-xs">
                          Editar
                        </Button>
                      </Link>
                      <Button
                        onClick={() => handleDeactivate(cliente.id)}
                        className="w-40 bg-red-200 text-red-600 hover:bg-red-600 hover:text-red-200 py-1 px-3 rounded-full text-xs"
                      >
                        Cambiar Estado
                      </Button>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VendedorIndex;
