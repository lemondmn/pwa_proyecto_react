import React from "react";
import axios from "../../api/axios";
import { Table } from "flowbite-react";
import NavbarVendedor from "./NavbarVendedor";

const Ganancia = () => {
  const [ganancias, setGanancias] = React.useState([]);

  React.useEffect(() => {
    getGanancias();
  });

  const getGanancias = async (e) => {
    try {
      const response = await axios.get(
        "/ganancias/" + localStorage.getItem("id"),
        {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
            "Content-Type": "application/json",
          },
        }
      );
      setGanancias(response.data.ganancias);
      console.log(response.data.ganancias);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <NavbarVendedor />
      <br />
      <div className="flex justify-center">
        <div className="block mx-6 w-auto p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
          <p className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            Ganancias
          </p>
        </div>
      </div>
      <br />
      <div className="flex w-full">
        <div className="w-full py-12 flex items-center justify-center">
          <div className="min-w-min px-12">
            <Table className="w-full table-fixed text-sm text-left text-gray-500 dark:text-gray-400">
              <Table.Head>
                <Table.HeadCell>Total</Table.HeadCell>
                <Table.HeadCell>Matriz</Table.HeadCell>
                <Table.HeadCell>Sucursal</Table.HeadCell>
                <Table.HeadCell>Vendedor</Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {ganancias.map((ganancia) => (
                  <Table.Row
                    key={ganancia.id}
                    className="bg-white dark:border-gray-700 dark:bg-gray-800"
                  >
                    <Table.Cell>{ganancia.total_ganancia}</Table.Cell>
                    <Table.Cell>{ganancia.matriz_ganancia}</Table.Cell>
                    <Table.Cell>{ganancia.sucursal_ganancia}</Table.Cell>
                    <Table.Cell>{ganancia.vendedor_ganancia}</Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Ganancia;
