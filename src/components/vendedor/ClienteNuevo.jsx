import React from "react";
import axios from "../../api/axios";
import { Card, Label, TextInput, Button } from "flowbite-react";
import { useNavigate } from "react-router-dom";
import NavbarVendedor from "./NavbarVendedor";

const ClienteNuevo = () => {
  const navigate = useNavigate();

  const [nombre, setNombre] = React.useState("");
  const [paterno, setPaterno] = React.useState("");
  const [materno, setMaterno] = React.useState("");
  const [direccion, setDireccion] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [telefono, setTelefono] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [vendedor, setVendedor] = React.useState("");

  React.useState(() => {
    setVendedor(localStorage.getItem("id"));
  });

  const handleSumbit = async (e) => {
    e.preventDefault();

    console.log("JSON" + JSON.stringify({ nombre, paterno, materno, direccion, telefono, email, password, vendedor}));
    try {
      const response = await axios.post(
        "/vendedores",
        JSON.stringify({ nombre, paterno, materno, direccion, telefono, email, password, vendedor}),
        { headers: { 
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
          "Content-Type": "application/json"
         } }
      );
      
      navigate("/vendedor/clientes", { replace: true });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <NavbarVendedor />
      <div className="flex w-full">
        <div className="w-full py-12 flex items-center justify-center">
          <div className="min-w-min w-3/4">
            <Card>
              <form onSubmit={handleSumbit} className="flex flex-col gap-4">
                <div>
                  <div className="mb-2 block">
                    <span className="mb-4 text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-pink-500 to-orange-400">
                      Cliente Nuevo
                    </span>
                  </div>
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="nombre" value="Nombre" />
                  </div>
                  <TextInput
                    id="nombre"
                    type="text"
                    onChange={(e) => {
                      setNombre(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="paterno" value="Apellido Paterno" />
                  </div>
                  <TextInput
                    id="paterno"
                    type="text"
                    onChange={(e) => {
                      setPaterno(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="materno" value="Apellido Materno" />
                  </div>
                  <TextInput
                    id="materno"
                    type="text"
                    onChange={(e) => {
                      setMaterno(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="direccion" value="Direccion" />
                  </div>
                  <TextInput
                    id="direccion"
                    type="text"
                    onChange={(e) => {
                      setDireccion(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="telefono" value="Numero Telefonico" />
                  </div>
                  <TextInput
                    id="telefono"
                    type="number"
                    onChange={(e) => {
                      setTelefono(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="email" value="Email" />
                  </div>
                  <TextInput
                    id="email"
                    type="email"
                    placeholder="example@a.a"
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="password" value="Contraseña" />
                  </div>
                  <TextInput
                    id="password"
                    type="password"
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <Button
                  type="submit"
                  className="text-white bg-gradient-to-br from-pink-500 to-orange-400 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                >
                  Agregar Cliente
                </Button>
              </form>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ClienteNuevo;
