import React from "react";
import axios from "../../api/axios";
import NavbarCliente from "./NavbarCliente";
import { Card, Label, TextInput } from "flowbite-react";

const Perfil = () => {

  const [nombre, setNombre] = React.useState("");
  const [paterno, setPaterno] = React.useState("");
  const [materno, setMaterno] = React.useState("");
  const [direccion, setDireccion] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [telefono, setTelefono] = React.useState("");

  React.useState(() => {
    const getClientById = async () => {
      const response = await axios.get("/cliente/info/" + localStorage.getItem('id'), {
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
          "Content-Type": "application/json",
        },
      });

      setNombre(response.data.cliente.nombre);
      setPaterno(response.data.cliente.apaterno);
      setMaterno(response.data.cliente.amaterno);
      setDireccion(response.data.cliente.direccion);
      setTelefono(response.data.cliente.telefono);
      setEmail(response.data.cliente.email);
    };
    getClientById();
  });

  return (
    <div>
      <NavbarCliente />
      <div className="flex w-full">
        <div className="w-full py-12 flex items-center justify-center">
          <div className="min-w-min w-3/4">
            <Card>
              <div className="flex flex-col gap-4">
                <div className="mb-2 block">
                  <span className="mb-4 text-5xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-pink-500 to-orange-400">
                    Perfil de Cliente
                  </span>
                </div>
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="nombre" value="Nombre" />
                </div>
                <TextInput
                  id="nombre"
                  type="text"
                  onChange={(e) => {
                    setNombre(e.target.value);
                  }}
                  value={nombre}
                  required={true}
                  readOnly={true}
                />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="paterno" value="Apellido Paterno" />
                </div>
                <TextInput
                  id="paterno"
                  type="text"
                  onChange={(e) => {
                    setPaterno(e.target.value);
                  }}
                  value={paterno}
                  required={true}
                  readOnly={true}
                />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="materno" value="Apellido Materno" />
                </div>
                <TextInput
                  id="materno"
                  type="text"
                  onChange={(e) => {
                    setMaterno(e.target.value);
                  }}
                  value={materno}
                  required={true}
                  readOnly={true}
                />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="direccion" value="Direccion" />
                </div>
                <TextInput
                  id="direccion"
                  type="text"
                  onChange={(e) => {
                    setDireccion(e.target.value);
                  }}
                  value={direccion}
                  required={true}
                  readOnly={true}
                />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="telefono" value="Numero Telefonico" />
                </div>
                <TextInput
                  id="telefono"
                  type="number"
                  onChange={(e) => {
                    setTelefono(e.target.value);
                  }}
                  value={telefono}
                  required={true}
                  readOnly={true}
                />
              </div>

              <div>
                <div className="mb-2 block">
                  <Label htmlFor="email" value="Email" />
                </div>
                <TextInput
                  id="email"
                  type="email"
                  placeholder="example@a.a"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  value={email}
                  required={true}
                  readOnly={true}
                />
              </div>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Perfil;
