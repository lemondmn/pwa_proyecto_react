import React from "react";
import axios from "../../api/axios";
import NavbarCliente from "./NavbarCliente";
import { Table } from "flowbite-react";

const Pagos = () => {
  const [pagados, setPagados] = React.useState([]);
  const [pendientes, setPendientes] = React.useState([]);

  React.useEffect(() => {
    getPagos();
  });

  const getPagos = async (e) => {
    try {
      const response = await axios.get(
        "/cliente/" + localStorage.getItem("id"),
        {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
            "Content-Type": "application/json",
          },
        }
      );
      setPagados(response.data.pagados);
      setPendientes(response.data.nopagados);
      console.log(pagados);
      console.log(pendientes);
      console.log(response.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <NavbarCliente />
      <br />
      <div className="flex justify-center">
        <div className="block mx-6 w-auto p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
          <p className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            Pagos
          </p>
        </div>
      </div>
      <br />

      {pagados.length > 0 ? (
        <div className="flex w-full">
          <div className="w-full py-12 flex items-center justify-center">
            <div className="min-w-min px-12">
              <div className="flex justify-center">
                <p>
                  <b>Pagos Realizados</b>
                </p>
              </div>
              <Table className="w-full table-fixed text-sm text-left text-gray-500 dark:text-gray-400">
                <Table.Head>
                  <Table.HeadCell>Monto</Table.HeadCell>
                  <Table.HeadCell>Fecha de Corte</Table.HeadCell>
                  <Table.HeadCell>Plan</Table.HeadCell>
                </Table.Head>
                <Table.Body className="divide-y">
                  {pagados.map((p) => (
                    <Table.Row
                      key={p.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell>{p.monto}</Table.Cell>
                      <Table.Cell>{p.fecha_corte}</Table.Cell>
                      <Table.Cell>{p.plan_id}</Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center">
          <div className="block mx-6 w-auto p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
            <p className="mb-4 text-xl font-extrabold text-gray-900 dark:text-white md:text-xl lg:text-6xl">
              No hay pagos realizados
            </p>
          </div>
        </div>
      )}

      {pendientes.length > 0 ? (
        <div className="flex w-full">
          <div className="w-full py-12 flex items-center justify-center">
            <div className="min-w-min px-12">
              <div className="flex justify-center">
                <p>
                  <b>Pagos Pendientes</b>
                </p>
              </div>
              <Table className="w-full table-fixed text-sm text-left text-gray-500 dark:text-gray-400">
                <Table.Head>
                  <Table.HeadCell>Monto</Table.HeadCell>
                  <Table.HeadCell>Fecha de Corte</Table.HeadCell>
                  <Table.HeadCell>Plan</Table.HeadCell>
                </Table.Head>
                <Table.Body className="divide-y">
                  {pagados.map((p) => (
                    <Table.Row
                      key={p.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell>{p.monto}</Table.Cell>
                      <Table.Cell>{p.fecha_corte}</Table.Cell>
                      <Table.Cell>{p.plan_id}</Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            </div>
          </div>
        </div>
      ) : (
        <div className="flex justify-center">
          <div className="block mx-6 w-auto p-6 bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
            <p className="mb-4 text-xl font-extrabold text-gray-900 dark:text-white md:text-xl lg:text-6xl">
              No hay pagos pendientes
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Pagos;
