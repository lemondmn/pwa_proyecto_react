import * as React from "react";
import logo from "../../logo.svg";
import { Card, Label, TextInput, Button } from "flowbite-react";
import { useNavigate, useLocation } from "react-router-dom";

import axios from "../../api/axios";

const Login = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");

  const handleSumbit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(
        "/login",
        JSON.stringify({ email, password }),
        { headers: { "Content-Type": "application/json" } }
      );
      const accessToken = response?.data?.token;
      const role = response?.data?.role;
      const userID = response?.data?.id;

      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("role", role);
      localStorage.setItem("id", userID);

      if (role === "vendedor") {
        navigate("/vendedor", { replace: true });
      } else if (role === "cliente") {
        navigate("/cliente", { replace: true });
      }
    } catch (err) {
      console.log(err);
    }
  };

  if (localStorage.getItem("accessToken")) {
    if(localStorage.getItem("role") === "vendedor") {
      navigate("/vendedor", { replace: true });
    } else if(localStorage.getItem("role") === "cliente") {
      navigate("/cliente", { replace: true });
    }
  } else {
    return (
      <div className="flex w-full h-screen bg-neutral-200">
        <div className="w-full flex items-center justify-center">
          <div className="min-w-min w-1/2">
            <img
              src={logo}
              className="max-w-xs mx-auto my-5 rounded-full bg-white"
              alt="logo"
            />
            <Card>
              <form onSubmit={handleSumbit} className="flex flex-col gap-4">
                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="email" value="Email" />
                  </div>
                  <TextInput
                    id="email"
                    type="email"
                    placeholder="example@a.a"
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    required={true}
                  />
                </div>
                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="password" value="Contraseña" />
                  </div>
                  <TextInput
                    id="password"
                    type="password"
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                    required={true}
                  />
                </div>

                <Button
                  type="submit"
                  className="text-white bg-gradient-to-br from-pink-500 to-orange-400 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                >
                  Iniciar Sesion
                </Button>
              </form>
            </Card>
          </div>
        </div>
      </div>
    );
  }
};

export default Login;
