import { useLocation, Navigate, Outlet } from "react-router-dom";

const RequireAuth = ({ role }) => {
  const location = useLocation();

  return(
    localStorage.getItem('role') === role
        ? <Outlet />
        : localStorage.getItem('accessToken')
            ? <Navigate to="/unauthorized" state={{ from: location }} />
            : <Navigate to="/login" state={{ from: location }} />
  )
};

export default RequireAuth;
