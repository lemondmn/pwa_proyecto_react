import React from "react";
import { Card } from "flowbite-react";

const Unauthorized = () => {

  return (
    <div className="flex w-full h-screen bg-neutral-200">
      <div className="w-full flex items-center justify-center">
        <div className="min-w-fit w-1/2">
          <Card>
            <div>
              <div
                className="flex p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800"
                role="alert"
              >
                <svg
                  aria-hidden="true"
                  className="flex-shrink-0 inline w-5 h-5 mr-3"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Info</span>
                <div>
                  <span className="font-medium">
                    No tienes permiso para ver esta pagina.
                  </span>
                </div>
              </div>
            </div>

            <div>
              <img
                src="https://http.cat/401"
                alt="Unauthorized"
                className="max-w-sm object-scale-down mx-auto my-5"
              />
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Unauthorized;
