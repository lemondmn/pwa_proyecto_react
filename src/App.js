import "./App.css";

import Login from "./components/login/Login";

import Unauthorized from "./components/auxiliar/Unauthorized";
import NotFound from "./components/auxiliar/NotFound";

import Dashboard from "./components/Dashboard";

import VendedorIndex from "./components/vendedor/VendedorIndex";
import ClienteNuevo from "./components/vendedor/ClienteNuevo";
import EditarCliente from "./components/vendedor/EditarCliente";
import Ganancia from "./components/vendedor/Ganancia";

import Perfil from "./components/cliente/Perfil";
import Pagos from "./components/cliente/Pagos";

import Layout from "./components/Layout";
import RequireAuth from "./components/utils/RequireAuth";

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        {/* Public */}
        <Route path="login" element={<Login />} />
        <Route path="unauthorized" element={ <Unauthorized /> } />

        {/* Protected */}

        <Route element={<RequireAuth role={"vendedor"} />}>
          {/* Vendedor */}
          <Route path="/vendedor" element={ <Dashboard /> } />
          <Route path="/vendedor/clientes" element={ <VendedorIndex /> } />
          <Route path="/vendedor/clientes/nuevo" element={ <ClienteNuevo /> } />
          <Route path="/vendedor/clientes/editar/:id" element={ <EditarCliente /> } />
          <Route path="/vendedor/ganancias" element={ <Ganancia /> } />
          
        </Route>
        
        <Route element={<RequireAuth role={"cliente"} />}>
          {/* Cliente */}
          <Route path="/cliente" element={ <Dashboard /> } />
          <Route path="/cliente/perfil" element={ <Perfil /> } />
          <Route path="/cliente/pagos" element={ <Pagos /> } />
        </Route>

        {/* Catch all */}
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
}

export default App;
